<?php
require_once('./../abstract/BaseRow.php');

class Category extends BaseRow
{
    private $id;
    private $name;

    public function __construct( $id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}

