<?php
abstract class BaseRow
{
    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    protected function setId( $id)
    {
        $this->id = $id;
    }
}
