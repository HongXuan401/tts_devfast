<?php
require_once('./../dao/Database.php');
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Accessotion.php');
class DatabaseDemo extends Database
{

    public function insertTableTest($name, $row)
    {
        $this->insertTable($name, $row);
    }


    public function selectTableTest( $name,  $where)
    {
        return $this->selectTable($name, $where);
    }


    public function updateTableTest( $name,  $row)
    {
        $this->updateTable($name, $row);
    }


    public function deleteTableTest( $name,  $row)
    {
        $this->deleteTable($name, $row);
    }

    public function truncateTableTest( $name)
    {
        $this->truncateTable($name);
    }


    public function updateTableByIdTest( $id,  $row)
    {
        $this->updateTableById($id, $row);
    }


    public function initDatabase()
    {

        for($i = 1; $i<=10 ; $i++)
        {
            $product = new Product($i, 'Bánh kẹo '.$i, 2);
            $this->insertTable('productTable', $product);

            $category = new Category($i, 'Danh mục '.$i);
            $this->insertTable('categoryTable', $category);

            $accessotion = new Accessotion($i, 'Accessotion '.$i);
            $this->insertTable('accessotionTable', $accessotion);
        }
    }

}

// $product = new Product(1, 'SamSung', 2);

$database = DatabaseDemo::getInstants('DatabaseDemo');

$database->initDatabase();
echo '<pre>';

// $database->insertTableTest('productTable',$product);
// $database->updateTableTest('productTable',$product);
// $database->deleteTableTest('productTable',$product);
// $database->truncateTableTest('productTable');
// $database->updateTableByIdTest('productTable', $product);

// pr_r($database->selectTableTest('categoryTable'));
// pr_r($database->selectTableTest('accessotionTable'));
print_r($database->selectTableTest('productTable'));

echo strtolower(get_class($database)).'Table';
