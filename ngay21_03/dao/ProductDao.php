<?php
require_once('./../abstract/BaseDao.php');
require_once('./../entity/Product.php');
require_once('./../entity/Category.php');
require_once('./../entity/Accessotion.php');

class ProductDao extends BaseDao
{
    /**
     * get Product by Name
     * @param  $name
     * @return mixed
     */
    public function findByName($name)
    {
        return $this->database->getTableByName('productTable', $name);
    }

    /**
     * get Product where
     * @return mixed
     */
    public function search()
    {
        return $this->database->selectTable('productTable');
    }
}

